/* eslint-disable no-undef */
const AWS = require('aws-sdk');
require('dotenv').config();

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
});

const imageLib = require('../src/index');

it('Check resize img', async () => {
  const url = 'https://test-image-upload-js.s3.amazonaws.com/Images/vscode.jpg';
  imageLib.init(
    process.env.AWS_ACCESS_KEY,
    process.env.AWS_SECRET_KEY,
    process.env.AWS_REGION_NAME,
    process.env.AWS_BUCKET_NAME,
  );
  await imageLib.resizeImage({
    url,
    width: 10,
    height: 200,
  });

  const s3 = new AWS.S3();
  const params = {
    Bucket: process.env.AWS_BUCKET_NAME,
    Key: 'resized/10x200vscode.jpg',
  };
  const response = await s3.headObject(params).promise();
  expect(response).toHaveProperty('ContentLength');
  expect(response.ContentLength).toBeGreaterThan(0);
});
