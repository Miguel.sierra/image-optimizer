const imageUtils = require('../src/index');
require('dotenv').config();

async function main() {
  try {
    // Initialize object
    imageUtils.init(
      process.env.AWS_ACCESS_KEY,
      process.env.AWS_SECRET_KEY,
      process.env.AWS_REGION_NAME,
      process.env.AWS_BUCKET_NAME,
    );

    await imageUtils.resizeImage({
      url: process.env.URL_TEST_IMAGE,
      width: 50,
      height: 50,
    });

    await imageUtils.greyConvert({
      url: process.env.URL_TEST_IMAGE,
    });

    await imageUtils.sepiaConvert({
      url: process.env.URL_TEST_IMAGE,
    });

    await imageUtils.scale({
      url: process.env.URL_TEST_IMAGE,
      scaleValue: 3,
    });
  } catch (error) {
    console.error(error);
  }
}

main();
