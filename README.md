# Image Utils with S3

Image Util is a NodeJS library for dealing with images.

Has operations like:

- Resize
- Scale
- Grayscale
- Sepia

## Installation

```npm
npm install
```

## Run

```npm
npm run dev
```

## Usage

```js

const imageUtils = require('imageutilss3jimp');


imageUtils.init(
    AWS_ACCESS_KEY,
    AWS_SECRET_KEY,
    AWS_REGION_NAME,
    AWS_BUCKET_NAME,
);

await imageUtils.greyConvert({
    url: 'url-image.jpg',
});

await imageUtils.resizeImage({
    url: 'url-image.jpg',
    width: 50,
    height: 50,
});

await imageUtils.sepiaConvert({
    url: 'url-image.jpg',
});

await imageUtils.scale({
    url: 'url-image.jpg',
    scaleValue: 3,
});





```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
