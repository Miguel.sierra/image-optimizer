/* eslint-disable implicit-arrow-linebreak */
const AWS = require('aws-sdk');
const Jimp = require('jimp');

/* ------------------------------------------------------------------------ */
/* Lib definition                                                    */
/* ------------------------------------------------------------------------ */

const s3Params = {};

const getFileName = (string) =>
  string.match(/[\w.$]+(?=png|jpg|gif|jpeg)\w+/g)[0];

module.exports.init = (accessKeyId, secretAccessKey, region, bucketName) => {
  AWS.config.update({
    accessKeyId,
    secretAccessKey,
  });
  s3Params.region = region;
  s3Params.bucketName = bucketName;
};

module.exports.greyConvert = async ({
  url,
  key = '',
  extension = 'jpg',
  ACL = 'public-read',
}) => {
  try {
    const image = await Jimp.read(url);
    const nameImage = getFileName(url);
    const newImage = image.grayscale();
    newImage.name = `${Math.random().toString(36).substring(50)}${extension}`;

    const imageMime = `image/${extension === 'jpg' ? 'jpeg' : extension}`;

    if (nameImage) {
      newImage.name = nameImage;
    }

    const bufferNewImage = await newImage.getBufferAsync(imageMime);

    const s3 = new AWS.S3();
    const s3UploadConfig = {
      ACL,
      Body: bufferNewImage,
      Bucket: s3Params.bucketName,
      ContentType: imageMime,
      Key: `${key}resized/gray${newImage.name}`,
    };
    return s3.putObject(s3UploadConfig).promise();
  } catch (error) {
    return new Error(error);
  }
};

module.exports.resizeImage = async ({
  url,
  width,
  height,
  key = '',
  extension = 'jpg',
  quality = false,
  ACL = 'public-read',
}) => {
  try {
    const image = await Jimp.read(url);
    const nameImage = getFileName(url);

    const newImage = image.resize(width, height);
    newImage.name = `${Math.random().toString(36).substring(50)}${extension}`;

    const imageMime = `image/${extension === 'jpg' ? 'jpeg' : extension}`;

    if (nameImage) {
      newImage.name = nameImage;
    }

    if (quality) {
      newImage.quality(quality);
    }

    const bufferNewImage = await newImage.getBufferAsync(imageMime);

    const s3 = new AWS.S3();
    const s3UploadConfig = {
      ACL,
      Body: bufferNewImage,
      Bucket: s3Params.bucketName,
      ContentType: imageMime,
      Key: `${key}resized/${width}x${height}${newImage.name}`,
    };
    return s3.putObject(s3UploadConfig).promise();
  } catch (error) {
    return new Error(error);
  }
};

module.exports.sepiaConvert = async ({
  url,
  key = '',
  extension = 'jpg',
  ACL = 'public-read',
}) => {
  try {
    const image = await Jimp.read(url);
    const nameImage = getFileName(url);
    const newImage = image.sepia();
    newImage.name = `${Math.random().toString(36).substring(50)}${extension}`;

    const imageMime = `image/${extension === 'jpg' ? 'jpeg' : extension}`;

    if (nameImage) {
      newImage.name = nameImage;
    }
    const bufferNewImage = await newImage.getBufferAsync(imageMime);

    const s3 = new AWS.S3();
    const s3UploadConfig = {
      ACL,
      Body: bufferNewImage,
      Bucket: s3Params.bucketName,
      ContentType: imageMime,
      Key: `${key}resized/sepia${newImage.name}`,
    };
    return s3.putObject(s3UploadConfig).promise();
  } catch (error) {
    return new Error(error);
  }
};

module.exports.scale = async ({
  url,
  scaleValue,
  key = '',
  extension = 'jpg',
  ACL = 'public-read',
}) => {
  try {
    if (scaleValue > 5) {
      return new Error('Scale value too big');
    }
    const image = await Jimp.read(url);
    const nameImage = getFileName(url);
    const newImage = image.scale(scaleValue);
    newImage.name = `${Math.random().toString(36).substring(50)}${extension}`;

    const imageMime = `image/${extension === 'jpg' ? 'jpeg' : extension}`;

    if (nameImage) {
      newImage.name = nameImage;
    }
    const bufferNewImage = await newImage.getBufferAsync(imageMime);

    const s3 = new AWS.S3();
    const s3UploadConfig = {
      ACL,
      Body: bufferNewImage,
      Bucket: s3Params.bucketName,
      ContentType: imageMime,
      Key: `${key}resized/scale${scaleValue}${newImage.name}`,
    };
    return s3.putObject(s3UploadConfig).promise();
  } catch (error) {
    return new Error(error);
  }
};
